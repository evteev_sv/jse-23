package com.nlmk.evteev;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListMarker {

    Logger logger = Logger.getLogger(ListMarker.class.getName());

    public List<List<Description>> makeList(List<Person> personList) {
        List<List<Description>> result = new ArrayList<>();
        if (personList.isEmpty()) {
            logger.log(Level.ALL, "Объект для просмотра не существует!");
            return Collections.emptyList();
        }
        Class aClass = personList.get(0).getClass();
        for (Person person : personList) {
            if (!aClass.equals(person.getClass())) {
                throw new IllegalArgumentException("Типы клакссов не совпадают!");
            }
            Class clazz = person.getClass();
            do {
                List<Description> descriptions = new ArrayList<>();
                parseClass(clazz.getDeclaredFields(), descriptions, person);
                if (!descriptions.isEmpty()) {
                    result.add(descriptions);
                }
                clazz = clazz.getSuperclass();
            } while (clazz.getSuperclass() != null);
        }
        return result;
    }

    private void parseClass(Field[] fields, List<Description> descriptions, Person person) {
        for (Field field : fields) {
            boolean hasValue = true;
            try {
                field.setAccessible(true);
                if (field.get(person) == null) {
                    hasValue = false;
                }
                descriptions.add(new Description(field.getName(), field.getType().toString(), hasValue));
            } catch (IllegalAccessException e) {
                logger.log(Level.ALL, e.getMessage());
            }
        }
    }
}
