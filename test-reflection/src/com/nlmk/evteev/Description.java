package com.nlmk.evteev;

public class Description {

    private String parameterName;
    private String parameterType;
    private Boolean hasValue;

    public Description(String parameterName, String parameterType, Boolean hasValue) {
        this.parameterName = parameterName;
        this.parameterType = parameterType;
        this.hasValue = hasValue;
    }

    @Override
    public String toString() {
        return "Description{" +
                "parameterName='" + parameterName + '\'' +
                ", parameterType='" + parameterType + '\'' +
                ", hasValue=" + hasValue +
                '}';
    }
}
