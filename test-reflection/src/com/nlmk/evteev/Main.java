package com.nlmk.evteev;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        ListMarker listMarker = new ListMarker();
        List<Person> list = new ArrayList<>();
        list.add(new Person("Petr", "Smirnov"));
        list.add(new Person("Ivan", "Fedorov", LocalDate.parse("15.04.1995", DateTimeFormatter.ofPattern("dd.MM.yyyy")), "test@mail.eu"));

        for (List<Description> descriptions : listMarker.makeList(list)) {
            System.out.println(descriptions.toString());
        }
    }
}
