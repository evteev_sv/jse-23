package com.nlmk.evteev;

import java.time.LocalDate;

public class Person {

    private String firstName;
    private String familyName;
    private LocalDate birthDate;
    private String email;

    public Person(String firstName, String familyName) {
        this.firstName = firstName;
        this.familyName = familyName;
    }

    public Person(String firstName, String familyName, LocalDate birthDate, String email) {
        this.firstName = firstName;
        this.familyName = familyName;
        this.birthDate = birthDate;
        this.email = email;
    }
}
